package com.bektursun.weatherapp.di.module

import com.bektursun.weatherapp.data.api.AppRemoteSource
import com.bektursun.weatherapp.data.api.OpenWeatherMapApi
import com.bektursun.weatherapp.data.api.UnsplashApi
import com.bektursun.weatherapp.utils.Constants
import com.bektursun.weatherapp.utils.Constants.NETWORK_TIMEOUT
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {

    single(override = true) {
        OkHttpClient().newBuilder()
            .connectTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(NETWORK_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(
                HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY)
            )
            .build()
    }

    single(override = true) {
        AppRemoteSource(get(), get())
    }

    single {
        get<Retrofit>().create(OpenWeatherMapApi::class.java)
    }

    single(override = true) {
        Retrofit.Builder()
            .baseUrl(Constants.UNSPLASH_BASE_URL)
            .client(get<OkHttpClient>())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    single {
        get<Retrofit>().create(UnsplashApi::class.java)
    }

}
