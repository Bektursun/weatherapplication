package com.bektursun.weatherapp.di.module

import com.bektursun.weatherapp.data.repository.Repository
import com.bektursun.weatherapp.data.repository.WeatherRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory<Repository> {
        WeatherRepository(get())
    }
}