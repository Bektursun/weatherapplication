package com.bektursun.weatherapp.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bektursun.weatherapp.R
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.*


@BindingAdapter("setWeekday")
fun setWeekday(textView: TextView, dates: String?) {

    val sdf = SimpleDateFormat("E hh:mm aaa", Locale.getDefault())
    val unixSeconds: Long = dates?.toLong()!!
    // convert seconds to milliseconds
    val date = Date(unixSeconds * 1000L)
    textView.text = sdf.format(date)
}

@BindingAdapter("setWeatherIcon")
fun setWeatherIcon(imageView: ImageView, url: String) {
    // https://openweathermap.org/img/wn/10d@2x.png
    Glide.with(imageView)
        .load("https://openweathermap.org/img/wn/$url.png")
        .centerCrop()
        .placeholder(R.drawable.ic_wb_sunny)
        .into(imageView)

}

@BindingAdapter("setRandomBackground")
fun setRandomBackground(view: ImageView, url: String?) {
    Glide.with(view)
        .load(url)
        .into(view)
}
