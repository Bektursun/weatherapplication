package com.bektursun.weatherapp

import android.app.Application
import com.bektursun.weatherapp.di.module.networkModule
import com.bektursun.weatherapp.di.module.repositoryModule
import com.bektursun.weatherapp.di.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(networkModule)
            modules(viewModelModule)
            modules(repositoryModule)
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}