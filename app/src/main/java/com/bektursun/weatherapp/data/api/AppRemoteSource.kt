package com.bektursun.weatherapp.data.api

class AppRemoteSource(private val api: OpenWeatherMapApi, private val unsplashApi: UnsplashApi) {

    fun getCurrentWeather(latitude: Double, longitude: Double) = api.getCurrentWeather(latitude = latitude, longitude =  longitude)

    fun getCurrentForecast(latitude: Double, longitude: Double) = api.getCurrentForecast(latitude = latitude, longitude =  longitude)

    fun getRandomPhotoFromUnsplash(query: String) = unsplashApi.getRandomPhotoFromUnsplash(query = query)
}