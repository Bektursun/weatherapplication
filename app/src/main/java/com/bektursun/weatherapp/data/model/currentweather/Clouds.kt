package com.bektursun.weatherapp.data.model.currentweather


import com.google.gson.annotations.SerializedName

data class Clouds(
    @SerializedName("all")
    val all: Int
)