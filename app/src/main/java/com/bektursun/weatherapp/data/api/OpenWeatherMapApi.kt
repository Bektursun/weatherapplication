package com.bektursun.weatherapp.data.api

import com.bektursun.weatherapp.data.model.currentweather.CurrentWeatherData
import com.bektursun.weatherapp.data.model.forecast.Forecast
import com.bektursun.weatherapp.data.model.unsplash.UnsplashRandomPhoto
import com.bektursun.weatherapp.utils.Constants
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface OpenWeatherMapApi {

    @GET
    fun getCurrentWeather(
        @Url url: String = "https://api.openweathermap.org/data/2.5/weather",
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("appid") api_key: String = Constants.API_KEY,
        @Query("units") units: String="metric"
    ): Observable<CurrentWeatherData>

    @GET
    fun getCurrentForecast(
        @Url url: String = "https://api.openweathermap.org/data/2.5/forecast",
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("appid") api_key: String = Constants.API_KEY,
        @Query("units") units: String="metric"
    ): Observable<Forecast>

}
