package com.bektursun.weatherapp.data.api

import com.bektursun.weatherapp.data.model.unsplash.UnsplashRandomPhoto
import com.bektursun.weatherapp.utils.Constants
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface UnsplashApi {

    @GET("photos/random")
    fun getRandomPhotoFromUnsplash(
        @Query("client_id")
        accessKey: String = Constants.UNSPLASH_ACCESS_KEY,
        @Query("orientation")
        orientation: String = "portrait",
        @Query("query")
        query: String
    ): Observable<UnsplashRandomPhoto>
}