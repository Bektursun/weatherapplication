package com.bektursun.weatherapp.data.repository

import com.bektursun.weatherapp.data.api.AppRemoteSource
import com.bektursun.weatherapp.data.model.currentweather.CurrentWeatherData
import com.bektursun.weatherapp.data.model.forecast.Forecast
import com.bektursun.weatherapp.data.model.unsplash.UnsplashRandomPhoto
import io.reactivex.Observable


data class Result<out T>(
    val data: T? = null,
    val error: Throwable? = null
)

fun <T> T.asResult(): Result<T> = Result(data = this, error = null)

open class WeatherRepository(private val api: AppRemoteSource) : Repository {

    override fun getCurrentData(
        latitude: Double,
        longitude: Double
    ): Observable<Result<CurrentWeatherData>> =
        api.getCurrentWeather(latitude, longitude)
            .flatMap {
                Observable.just(it.asResult())
            }
            .onErrorResumeNext { t: Throwable ->
                return@onErrorResumeNext Observable.just(
                    Result(null, t)
                )
            }

    override fun getCurrentForecast(
        latitude: Double,
        longitude: Double
    ): Observable<Result<Forecast>> =
        api.getCurrentForecast(latitude, longitude)
            .flatMap {
                Observable.just(it.asResult())
            }
            .onErrorResumeNext { t: Throwable ->
                return@onErrorResumeNext Observable.just(
                    Result(null, t)
                )
            }

    override fun getRandomPhotoFromUnsplash(query: String): Observable<Result<UnsplashRandomPhoto>> =
        api.getRandomPhotoFromUnsplash(query = query)
            .flatMap {
                Observable.just(it.asResult())
            }
            .onErrorResumeNext { t: Throwable ->
                return@onErrorResumeNext Observable.just(
                    Result(
                        null, t
                    )
                )
            }
}