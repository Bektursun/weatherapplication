package com.bektursun.weatherapp.data.repository

import com.bektursun.weatherapp.data.model.currentweather.CurrentWeatherData
import com.bektursun.weatherapp.data.model.forecast.Forecast
import com.bektursun.weatherapp.data.model.unsplash.UnsplashRandomPhoto
import io.reactivex.Observable

interface Repository {

    fun getCurrentData(latitude: Double, longitude: Double): Observable<Result<CurrentWeatherData>>

    fun getCurrentForecast(latitude: Double, longitude: Double): Observable<Result<Forecast>>

    fun getRandomPhotoFromUnsplash(query: String): Observable<Result<UnsplashRandomPhoto>>
}
