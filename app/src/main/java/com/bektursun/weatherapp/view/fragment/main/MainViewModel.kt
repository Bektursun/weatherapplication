package com.bektursun.weatherapp.view.fragment.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bektursun.weatherapp.base.BaseViewModel
import com.bektursun.weatherapp.data.model.currentweather.CurrentWeatherData
import com.bektursun.weatherapp.data.model.forecast.Forecast
import com.bektursun.weatherapp.data.model.forecast.X
import com.bektursun.weatherapp.data.model.unsplash.UnsplashRandomPhoto
import com.bektursun.weatherapp.data.repository.Repository
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class MainViewModel(private val repository: Repository) : BaseViewModel() {

    private val _currentWeather: MutableLiveData<CurrentWeatherData> = MutableLiveData()
    val currentWeatherData: LiveData<CurrentWeatherData>
        get() = _currentWeather

    private val _currentForecast: MutableLiveData<Forecast> = MutableLiveData()
    val currentForecast: LiveData<Forecast>
        get() = _currentForecast

    private val _forecast: MutableLiveData<List<X>> = MutableLiveData()
    val forecast: LiveData<List<X>>
        get() = _forecast

    private val _isProgress: MutableLiveData<Boolean> = MutableLiveData()
    val isProgress: LiveData<Boolean>
        get() = _isProgress

    private val _randomPhoto: MutableLiveData<UnsplashRandomPhoto> = MutableLiveData()
    val randomPhoto: LiveData<UnsplashRandomPhoto>
        get() = _randomPhoto

    fun getCurrentWeather(latitude: Double, longitude: Double) {
        val currentWeather = repository.getCurrentData(latitude, longitude)
            .observeOn(Schedulers.io())
            .doOnSubscribe { _isProgress.postValue(true) }
            .doOnTerminate { _isProgress.postValue(false) }
            .subscribe({ result -> _currentWeather.postValue(result.data) },
                { e -> Timber.e("$e") })
        compositeDisposable.add(currentWeather)
    }

    fun getCurrentForecast(latitude: Double, longitude: Double) {
        val currentForecast = repository.getCurrentForecast(latitude, longitude)
            .observeOn(Schedulers.io())
            .subscribe ({ result -> postForecastDataForRV(result.data!!) },
                { t -> Timber.e(t.localizedMessage) })
        compositeDisposable.add(currentForecast)
    }

    fun getRandomPhotoFromUnsplash(query: String) {
        val currentRandomPhoto = repository.getRandomPhotoFromUnsplash(query = query)
            .observeOn(Schedulers.io())
            .subscribe({ result -> _randomPhoto.postValue(result.data) },
                { t -> Timber.e(t.localizedMessage) })
        compositeDisposable.add(currentRandomPhoto)
    }

    private fun postForecastDataForRV(data: Forecast) {
        _currentForecast.postValue(data)
        _forecast.postValue(data.list)
    }

}
