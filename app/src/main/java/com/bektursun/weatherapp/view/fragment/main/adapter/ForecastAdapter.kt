package com.bektursun.weatherapp.view.fragment.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bektursun.weatherapp.data.model.forecast.X
import com.bektursun.weatherapp.databinding.ForecastItemBinding
import timber.log.Timber

class ForecastAdapter : ListAdapter<X, ForecastAdapter.ViewHolder>(ForecastDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        Timber.i("onCreateViewHolder")
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Timber.i("onBindViewHolder")
        val weatherForecast = getItem(position)
        holder.bind(weatherForecast)
    }

    class ViewHolder(private val binding: ForecastItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(weatherForecast: X) {
            binding.apply {
                Timber.i("ViewHolder BIND()")
                forecast = weatherForecast
                val weatherDescription = weatherForecast.weather?.first()
                description = weatherDescription
                executePendingBindings()
            }
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ForecastItemBinding.inflate(layoutInflater, parent, false)
                return ViewHolder(binding)
            }
        }
    }

    class ForecastDiffCallback : DiffUtil.ItemCallback<X>() {

        override fun areItemsTheSame(oldItem: X, newItem: X): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: X, newItem: X): Boolean {
            return oldItem == newItem
        }
    }
}