package com.bektursun.weatherapp.view.fragment.main

import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.bektursun.weatherapp.R
import com.bektursun.weatherapp.base.BaseFragment
import com.bektursun.weatherapp.data.model.currentweather.CurrentWeatherData
import com.bektursun.weatherapp.data.model.forecast.Forecast
import com.bektursun.weatherapp.data.model.forecast.X
import com.bektursun.weatherapp.data.model.unsplash.UnsplashRandomPhoto
import com.bektursun.weatherapp.databinding.MainFragmentBinding
import com.bektursun.weatherapp.utils.Permission
import com.bektursun.weatherapp.utils.observe
import com.bektursun.weatherapp.view.fragment.main.adapter.ForecastAdapter
import com.google.android.gms.location.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class MainFragment : BaseFragment() {

    private lateinit var binding: MainFragmentBinding
    private val viewModel by viewModel<MainViewModel>()

    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null

    private lateinit var forecastAdapter: ForecastAdapter

    private var latitude: Double = 0.0
    private var longitude: Double = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.i("onCreateView()")
        binding = DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        forecastAdapter = ForecastAdapter()


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.i("onViewCreated()")

        val bottomSheetBehavior = BottomSheetBehavior.from(binding.bottomSheet)
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                Timber.i("onSlide() $slideOffset")
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                Timber.i("onStateChanged $newState")
                when(newState) {
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        binding.swipeIcon.setImageResource(R.drawable.ic_arrow_drop_down)
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        binding.swipeIcon.setImageResource(R.drawable.ic_arrow_drop_up)
                    }
                    else -> return
                }
            }

        })

        setupRV()
    }

    private fun setupRV() {
        val recyclerView = binding.forecastRV
        recyclerView.adapter = forecastAdapter
    }

    override fun observeChange() {
        observe(viewModel.currentWeatherData, ::fillCurrentWeather)
        observe(viewModel.currentForecast, ::fillCurrentForecast)
        observe(viewModel.forecast, ::fillRV)
        observe(viewModel.isProgress, ::isProgress)
        observe(viewModel.randomPhoto, ::getRandomPhoto)
    }

    private fun getRandomPhoto(data: UnsplashRandomPhoto) {
        binding.randomImage = data
        Timber.i(data.description)
    }

    private fun isProgress(isProgress: Boolean) {
        when(isProgress) {
            true -> binding.progressBar.visibility = View.VISIBLE
            else -> binding.progressBar.visibility = View.GONE
        }
    }

    private fun fillRV(data: List<X>) {
        forecastAdapter.submitList(data)
    }

    @SuppressLint("SetTextI18n")
    private fun fillCurrentWeather(data: CurrentWeatherData) {
        Timber.i("FILL DATA")
        with(binding) {
            cityNameTextView.text = data.name
            currentCityTemp.text = data.main.tempMax.toInt().toString() + "\u2103"
            humidityTextView.text = data.main.humidity.toString() + "%"
            windTextView.text = data.wind.speed.toString() + "m/s"
            currentCityDescription.text = data.weather.first().description
        }
        viewModel.getRandomPhotoFromUnsplash(query = data.weather.first().description)
    }

    private fun fillCurrentForecast(forecast: Forecast) {
        forecast.list?.forEach { println(it) }
    }

    override fun onResume() {
        super.onResume()
        Timber.i("onResume() $latitude, $longitude")
    }

    override fun onStart() {
        super.onStart()
        when {
            Permission.isAccessFineLocationGranted(requireActivity()) -> {
                when {
                    Permission.isLocationEnabled(requireActivity()) -> {
                        setUpLocationListener()
                    }
                    else -> {
                        Permission.showGPSNotEnabledDialog(requireActivity())
                    }
                }
            }
            else -> {
                Permission.requestAccessFineLocationPermission(
                    requireActivity(),
                    1
                )
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun setUpLocationListener() {
        val fusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireActivity())
        locationRequest = LocationRequest()
            .setInterval(1000000)
            .setFastestInterval(100000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                Timber.i("Location: ${locationResult.locations[0].latitude}, ${locationResult.locations[0].longitude}")
                latitude = locationResult.locations[0].latitude
                longitude = locationResult.locations[0].longitude
                viewModel.getCurrentWeather(latitude, longitude)
                viewModel.getCurrentForecast(latitude, longitude)
            }
        }

        fusedLocationProviderClient?.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null /* Looper */
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        mFusedLocationClient.removeLocationUpdates(locationCallback)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        Permission.isLocationEnabled(requireActivity()) -> {
                            setUpLocationListener()
                        }
                        else -> {
                            Permission.showGPSNotEnabledDialog(requireActivity())
                        }
                    }
                } else {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.location_permission_not_granted),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }
}
